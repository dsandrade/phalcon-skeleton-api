<?php

namespace Phalcon\Skeleton\Api\Controllers;

use Phalcon\DI;
use Phalcon\DI\Injectable;

/**
 *  \Phalcon\Mvc\Controller has a final __construct() method, so we can't
 *  extend the constructor (which we will need for our RESTController).
 *  Thus we extend DI\Injectable instead.
 */
class BaseController extends Injectable
{
    /**
     * BaseController constructor.
     */
    public function __construct()
    {
        $diContainer = DI::getDefault();
        $this->setDI($diContainer);
    }
}
