<?php

namespace Phalcon\Skeleton\Api;

use Phalcon\Version;
use Phalcon\Skeleton\Api\Models\Keys;

class CoreController extends BaseController
{
    /**
     * @return mixed
     */
    public function index()
    {     
        $keys                   = new Keys();
        $keys->rebuildKeyStack(240);
        $info                   = [];
        $info['app']            = 'REST Api @Phalcon ' . Version::get();
        $info['sha1(password)'] = sha1('password');
        $info['keys']           = $this->getKeys();
        $info['passwords']      = $this->getPasswords($keys);
        
        $this->response->setJsonContent($info);
        return $this->response->send();
    }

    /**
     * @param $keys
     * @return array
     */
    private function getPasswords($keys): array
    {
        $passwords = [];
        foreach ($keys as $key) {
            $password = [];
            $password['sha1(sha1(password) + key)'] = sha1(sha1('password') . $key['key']);
            $password['lifetime'] = $key['lifetime']; 
            $passwords[] = $password;
        }

        return $passwords;
    }

    /**
     * @return array
     */
    private function getKeys(): array
    {    
        $keys = new Keys(); 
        return $keys->getValidList();
    }
}
