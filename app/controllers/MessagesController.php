<?php

namespace Phalcon\Skeleton\Api\Controllers;

use Phalcon\Skeleton\Api\Models\Messages;

class MessagesController extends BaseController
{
    /**
     * Create new message
     * 
     * @return mixed
     */
    public function create()
    {
        if (!empty($this['auth']['id'])) {
            $request = $this->request->getJsonRawBody();

            if (($request !== null) && ($request->id_receiver && $request->content && isset($request->type))) {
                $msg = new Messages();
                $message = $msg->setSingle(
                    $this['auth']['id'],
                    $request->id_receiver,
                    $request->content,
                    $request->type
                );

                $this->response
                    ->setStatusCode(201, 'Created')
                    ->setJsonContent([
                        'status' => 'OK',
                        'data' => $message
                    ]);
            } else {
                $this->response
                    ->setStatusCode(400, 'Bad Request')
                    ->setJsonContent([
                        'status' => 'ERROR',
                        'data' => 'wrong JSON input'
                    ]);
            }
        } else {
            $this->response
                ->setStatusCode(401, 'Unauthorized')
                ->setJsonContent([
                    'status' => 'ERROR',
                    'data'   => 'Access is not authorized'
                ]);
        }

        return $this->response;
    }

    /**
     * List messages sent from one user to another
     * 
     * @param integer $id_sender
     * @param integer $id_receiver
     * @return mixed
     */
    public function stream($id_sender, $id_receiver)
    {
        if (!empty($this['auth']['id'])
            && ($this['auth']['id'] === $id_sender
                || $this['auth']['id'] === $id_receiver)) {
            $msg = new Messages();
            $offset = $this->request->getQuery('offset');
            $limit = $this->request->getQuery('limit');
            $this->response->setJsonContent($msg->getFlow(
                $id_sender,
                $id_receiver,
                $offset,
                $limit
            ));

            return $this->response;
        }

        return $this->response
            ->setStatusCode(401, 'Unauthorized')
            ->setJsonContent([
                'status' => 'ERROR',
                'data' => 'Access is not authorized'
            ]);
    }

    /**
     * List latest messages
     * 
     * @return mixed
     */
    public function inbox()
    {
        if (!empty($this['auth']['id'])) {
            $msg = new Messages();
            $offset = $this->request->getQuery('offset');
            $limit = $this->request->getQuery('limit');
            $this->response->setJsonContent($msg->getInbox(
                $this['auth']['id'],
                $offset,
                $limit
            ));

            return $this->response;
        }

        return $this->response
            ->setStatusCode(401, 'Unauthorized')
            ->setJsonContent([
                'status' => 'ERROR',
                'data' => 'Access is not authorized'
            ]);
    }
}
