<?php

namespace Phalcon\Skeleton\Api\Controllers;

use Phalcon\Skeleton\Api\Models\Users;

class UsersController extends BaseController
{
    /**
     * Return list of Users
     * 
     * @return mixed
     */
    public function preview()
    {
        $user = new Users();
        $this->response->setJsonContent($user->getList());
        return $this->response->send();
    }

    /**
     * Create new User
     * 
     * @return mixed
     */
    public function create()
    {
        $request = $this->request->getJsonRawBody();

        if (($request !== null) && ($request->login && $request->password && $request->pubkey)) {
            $user = new Users();
            $userId = $user->setUser(
                $request->login,
                $request->password,
                $request->pubkey
            );

            if ($userId) {
                $this->response
                    ->setStatusCode(201, 'Created')
                    ->setJsonContent([
                        'status' => 'OK',
                        'data' => $userId
                    ]);
            } else {
                $this->response
                    ->setStatusCode(409, 'Conflict')
                    ->setJsonContent([
                        'status' => 'ERROR',
                        'data' => 'duplicated User login'
                    ]);
            }
        } else {
            $this->response
                ->setStatusCode(400, 'Bad Request')
                ->setJsonContent([
                    'status' => 'ERROR',
                    'data' => 'wrong JSON input'
                ]);
        }

        return $this->response->send();
    }

    /**
     * Return detailed data about User
     * 
     * @param integer $id
     * @return mixed
     */
    public function info($id)
    {
        $user = Users::findFirst($id);
        if ($user) {
            $this->response->setJsonContent($user->getUser());
        } else {
            $this->response
                ->setStatusCode(404, 'Not Found')
                ->setJsonContent([
                    'status' => 'ERROR',
                    'data' => 'User not found'
                ]);
        }

        return $this->response->send();
    }

    /**
     * Update User data
     * 
     * @param integer $id
     * @return mixed
     */
    public function update($id)
    {
        if (!empty($this['auth']['id']) && ($this['auth']['id'] === $id)) {
            $request = $this->request->getJsonRawBody();

            if (($request !== null) && ($request->login && $request->password && $request->pubkey && $request->old_password)) {
                $user = Users::findFirst($id);
                $userId = $user->updateUser(
                    $request->login,
                    $request->password,
                    $request->pubkey,
                    $request->old_password
                );

                if ($userId) {
                    $this->response
                        ->setJsonContent([
                            'status' => 'OK',
                            'data' => $userId
                        ]);
                } else {
                    $this->response
                        ->setStatusCode(409, 'Conflict')
                        ->setJsonContent([
                            'status' => 'ERROR',
                            'data' => 'Validation (2nd level) fail or data error'
                        ]);
                }
            } else {
                $this->response
                    ->setStatusCode(400, 'Bad Request')
                    ->setJsonContent([
                        'status' => 'ERROR',
                        'data' => 'wrong JSON input'
                    ]);
            }
        } else {
            $this->response
                ->setStatusCode(401, 'Unauthorized')
                ->setJsonContent([
                    'status' => 'ERROR',
                    'data' => 'Access is not authorized'
                ]);
        }

        return $this->response->send();
    }

    /**
     * Delete User
     * 
     * @param integer $id
     * @return mixed
     */
    public function delete($id)
    {
        if (!empty($this['auth']['id']) && ($this['auth']['id'] === $id)) {
            $request = $this->request->getJsonRawBody();
            $user = Users::findFirst($id);
            if (!empty($request->old_password) && ($request->old_password === $user->getPassword())) {
                if ($user->delete() === false) {
                    $this->response
                        ->setStatusCode(400, 'Bad Request')
                        ->setJsonContent([
                            'status' => 'ERROR',
                            'data' => 'User not deleted'
                        ]);
                } else {
                    $this->response
                        ->setStatusCode(204, 'OK')
                        ->setJsonContent([
                            'status' => 'OK',
                            'data'   => 'User deleted'
                        ]);
                }
            } else {
                $this->response
                    ->setStatusCode(401, 'Unauthorized')
                    ->setJsonContent([
                        'status' => 'ERROR',
                        'data'   => 'Validation (2nd level) fail'
                    ]);
            }
        } else {
            $this->response
                ->setStatusCode(401, 'Unauthorized')
                ->setJsonContent([
                    'status' => 'ERROR',
                    'data' => 'Access is not authorized'
                ]);
        }

        return $this->response->send();
    }
}
