<?php

namespace Phalcon\Skeleton\Api\Models;

use Phalcon\Mvc\Model;

class Keys extends Model
{

    /**
     * @var integer
     */
    protected $id;

    /**
     * @var integer
     */
    protected $time;

    /**
     * @var integer
     */
    protected $valid;
    
    /**
     * @var string
     */
    protected $key;

    /**
     * @param $lifetime
     */
    public function rebuildKeyStack($lifetime): void
    {
        $key = self::findFirst([
            'valid = "1"',
            'order' => 'time ASC'
        ]);
                
        if (!empty($key) && ($key->time < time())) {
            $key->valid = 0;
            $key->save();
        } 
        
        if (\count($this->getValidList()) < 2) {
            $this->generateNewKey($lifetime);
        }
    }

    /**
     * @param $lifetime
     */
    protected function generateNewKey($lifetime): void
    {
        $k = new self();
            $k->time = time() + $lifetime;
            $k->valid = 1;
            $k->key = sha1(rand() . time());
            $k->save();
    }

    /**
     * @return array
     */
    public function getValidList(): array
    {
        $keys = self::find([
            'valid = "1"',
            'order' => 'time DESC'
        ]);
        
        $vKeys = [];
        
        foreach ($keys as $key) {
            $vKey = [];
            $vKey['key'] = $key->key;
            $vKey['lifetime'] = $key->time - time();
            $vKeys[] = $vKey;
        }
        
        return $vKeys;
    }
}
