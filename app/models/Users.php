<?php

namespace Phalcon\Skeleton\Api\Models;

use Phalcon\Mvc\Model;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Uniqueness;

class Users extends Model
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $login;

    /**
     * @var string
     */
    protected $password;

    /**
     * @var string
     */
    protected $pubkey;

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return bool
     */
    public function validation(): bool
    {
        $validation = new Validation();

        $validation->add(
            'login',
            new Uniqueness([
                'model'     => new self(),
                'message' => ':field must be unique',
                'attribute' => 'login'
            ])
        );

        $messages = $validation->validate([
            'login'    => $this->login,
            'password' => $this->password,
            'pubkey'   => $this->pubkey
        ]);

        if (\count($messages)) {
            return false;
        }

        return true;
    }

    /**
     * @param $login
     * @param $password
     * @param $pubkey
     * @return int|null
     */
    public function setUser($login, $password, $pubkey): ?int
    {
        $this->login    = $login;
        $this->password = $password;
        $this->pubkey   = $pubkey;

        if ($this->validation()) {
            $this->save();
        }

        return $this->id;
    }

    /**
     * @param $login
     * @param $password
     * @param $pubkey
     * @param $old_password
     * @return int|null
     */
    public function updateUser($login, $password, $pubkey, $old_password): ?int
    {
        $secretOldPassword = $this->password;
        $this->login = $login;
        $this->password = $password;
        $this->pubkey = $pubkey;

        if (($secretOldPassword === $old_password) && $this->validation()) {
            $this->save();

            return $this->id;
        }

        return null;
    }

    /**
     * @return array
     */
    public function getUser(): array
    {
        $return_user           = [];
        $return_user['id']     = $this->id;
        $return_user['login']  = $this->login;
        $return_user['pubkey'] = $this->pubkey;

        return $return_user;
    }

    /**
     * @return array
     */
    public function getList(): array
    {
        $return_users = [];

        foreach (self::find() as $user) {
            $return_user          = [];
            $return_user['id']    = $user->id;
            $return_user['login'] = $user->login;
            $return_users[]       = $return_user;
        }

        return $return_users;
    }

    /**
     * Password sent by user must be:
     * sha1(saved_hash + Y-m-d)
     * 
     * @param $login
     * @param $password
     * @return int|null
     */
    public function getUserId($login, $password): ?int
    {
        $user = self::findFirst("login = '" . $login . "'");

        if ($user !== false) {
            $keys = new Keys();
            $vKeys = [];
            foreach ($keys->getValidList() as $key) {
                $vKeys[] = sha1($user->password . $key['key']);
            }

            if (\in_array($password, $vKeys, false)) {
                return $user->id;
            }
        }

        return null;
    }
}
