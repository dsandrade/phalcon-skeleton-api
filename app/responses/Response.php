<?php

use Phalcon\DI;
use Phalcon\DI\Injectable;

class Response extends Injectable
{
    /**
     * @var bool
     */
    protected $head = false;

    /**
     * Response constructor.
     */
    public function __construct()
    {
        $di = DI::getDefault();
        $this->setDI($di);
        if (strtolower($this->di->get('request')->getMethod()) === 'head') {
            $this->head = true;
        }
    }

    /**
     * In-Place, recursive conversion of array keys in snake_Case to camelCase
     * @param  array $snakeArray Array with snake_keys
     * @return no return value, array is edited in place
     */
    protected function arrayKeysToSnake($snakeArray)
    {
        foreach($snakeArray as $key => $value) {
            if (is_array($key)) {
                $value = $this->arrayKeysToSnake($value);
            }

            $snakeArray[$this->snakeToCamel($key)] = $value;
            if ($this->snakeToCamel($key) != $key) {
                unset($snakeArray[$key]);
            }
        }

        return $snakeArray;
    }

    /**
     * Replaces underscores with spaces, upper cases the first letters of each word,
     * lower cases the very first letter, then strips the spaces
     * @param string $val String to be converted
     * @return string     Converted string
     */
    protected function snakeToCamel($val): string
    {
        return str_replace(
            ' ',
            '',
            lcfirst(ucwords(str_replace('_', ' ', $val)))
        );
    }
}
