<?php

use Dotenv\Dotenv;
use Phalcon\Loader;
use Phalcon\Mvc\Micro;
use Phalcon\DI\FactoryDefault;
use Phalcon\Skeleton\Api\Models\Users;
use Phalcon\Session\Adapter\Files;
use Phalcon\Skeleton\Api\Exceptions\HTTPException;
use Phalcon\Db\Adapter\Pdo\Mysql as MysqlAdapter;
use Phalcon\Db\Adapter\Pdo\Postgresql as PostgresAdapter;

$dotEnv = new Dotenv(__DIR__ . '/../');
$dotEnv->load();

// Setup loader
$loader = new Loader();
$loader->registerNamespaces([
    'Phalcon\Skeleton\Api\Controllers' => __DIR__ . '/app/controllers/',
    'Phalcon\Skeleton\Api\Models'      => __DIR__ . '/app/models/',
    'Phalcon\Skeleton\Api\Exceptions'  => __DIR__ . '/app/exceptions/',
    'Phalcon\Skeleton\Api\Responses'   => __DIR__ . '/app/responses/'
])->register();

$databaseConfigs = require __DIR__ . '/../configs/database.php';

$diContainer = new FactoryDefault();

/**
 * Return array of the Collections, which define a group of routes, from
 * routes/collections.  These will be mounted into the app itself later.
 */
$diContainer->set('collections', function() {
    return require __DIR__ . '/routeLoader.php';
});

/**
 * $di's setShared method provides a singleton instance.
 * If the second parameter is a function, then the service is lazy-loaded
 * on its first instantiation.
 */
$diContainer->setShared('configs', function() {
    return require __DIR__ . '/../configs/app.php';
});
$diContainer->setShared('databaseConfigs', function() use ($databaseConfigs) {
    return $databaseConfigs;
});
// As soon as we request the session service, it will be started.
$diContainer->setShared('session', function(){
    $session = new Files();
    $session->start();
    return $session;
});

/**
 * Database setup.  Here, we'll use a simple SQLite database of Disney Princesses.
 */
$diContainer->set('db', function() use ($databaseConfigs) {
    if ($databaseConfigs->driver === 'mysql') {
        return new MysqlAdapter([
            'host'     => $databaseConfigs->host,
            'username' => $databaseConfigs->username,
            'password' => $databaseConfigs->password,
            'dbname'   => $databaseConfigs->database
        ]);
    } elseif ($databaseConfigs->driver === 'pgsql') {
        return new PostgresAdapter([
            'host'     => $databaseConfigs->host,
            'username' => $databaseConfigs->username,
            'password' => $databaseConfigs->password,
            'dbname'   => $databaseConfigs->database
        ]);
    }
});

// Start Micro
$app = new Micro();
$app->setDI($diContainer);


// Include controllers
$app['controllers'] = function() {
    return [
        'core' => true,
        'users' => true,
        'messages' => true
    ];
};

// Authentication
$app['auth'] = function() use ($app) {
    $auth = [];
    $authorization = $app->request->getHeader('AUTHORIZATION');
    if ($authorization) {
        $cut = str_replace('Basic ', '', $authorization);
        $creds = explode(':', base64_decode($cut));
        $auth['login'] = $creds[0];
        $auth['password'] = $creds[1];
    } else {
        $auth['login'] = null;
        $auth['password'] = null;
    }

    $usr = new Users();
    $auth['id'] = $usr->getUserId($auth['login'], $auth['password']);

    return $auth;
};

/**
 * Mount all of the collections, which makes the routes active.
 */
foreach($diContainer->get('collections') as $collection) {
    $app->mount($collection);
}

/**
 * The base route return the list of defined routes for the application.
 * This is not strictly REST compliant, but it helps to base API documentation off of.
 * By calling this, you can quickly see a list of all routes and their methods.
 */
$app->get('/', function() use ($app) {
    $routes = $app->getRouter()->getRoutes();
    $routeDefinitions = [
        'GET'     => [],
        'POST'    => [],
        'PUT'     => [],
        'PATCH'   => [],
        'DELETE'  => [],
        'HEAD'    => [],
        'OPTIONS' => []
    ];

    foreach($routes as $route) {
        $method = $route->getHttpMethods();
        $routeDefinitions[$method][] = $route->getPattern();
    }

    return $routeDefinitions;
});

/**
 * After a route is run, usually when its Controller returns a final value,
 * the application runs the following function which actually sends the response to the client.
 *
 * The default behavior is to send the Controller's returned value to the client as JSON.
 * However, by parsing the request query string's 'type' parameter, it is easy to install
 * different response type handlers.  Below is an alternate csv handler.
 */
$app->after(function() use ($app) {
    // OPTIONS have no body, send the headers, exit
    if ($app->request->getMethod() === 'OPTIONS') {
        $app->response->setStatusCode('200', 'OK');
        $app->response->send();
        return;
    }

    // Respond by default as JSON
    if (!$app->request->get('type') || $app->request->get('type') === 'json') {
        // Results returned from the route's controller.  All Controllers should return an array
        $records = $app->getReturnedValue();
        $response = new JSONResponse();
        $response->useEnvelope(true) //this is default behavior
        ->convertSnakeCase(true) //this is also default behavior
        ->send($records);
        return;
    } elseif ($app->request->get('type') === 'csv') {
        $records = $app->getReturnedValue();
        $response = new CSVResponse();
        $response->useHeaderRow(true)->send($records);
        return;
    } else {
        throw new HTTPException(
            'Could not return results in specified format',
            403,
            [
                'dev' => 'Could not understand type specified by type parameter in query string.',
                'internalCode' => 'NF1000',
                'more' => 'Type may not be implemented. Choose either "csv" or "json"'
            ]
        );
    }
});

/**
 * The notFound service is the default handler function that runs when no route was matched.
 * We set a 404 here unless there's a suppress error codes.
 */
$app->notFound(function () {
    throw new HTTPException('Not Found.', 404, [
        'dev' => 'That route was not found on the server.',
        'internalCode' => 'NF1000',
        'more' => 'Check route for misspellings.'
    ]);
});

/**
 * If the application throws an HTTPException, send it on to the client as json.
 * Elsewise, just log it.
 * TODO:  Improve this.
 */
set_exception_handler(function($exception) {
    //HTTPException's send method provides the correct response headers and body
    if (is_a($exception, HTTPException::class)) {
        $exception->send();
    }

    error_log($exception);
    error_log($exception->getTraceAsString());
});

return $app;
