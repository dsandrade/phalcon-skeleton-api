<?php

/**
 * routeLoader loads a set of Phalcon Mvc\Micro\Collections from
 * the collections directory.
 *
 * php files in the collections directory must return Collection objects only.
 */
return call_user_func(function() {
    $collections = [];
    foreach(scandir(__DIR__ . '/../routes/api/v1/') as $collectionFile) {
        $pathInfo = pathinfo($collectionFile);
        //Only include php files
        if ($pathInfo['extension'] === 'php') {
            // The collection files return their collection objects, so mount
            // them directly into the router.
            $collections[] = require __DIR__ . '/../routes/api/v1/' . $collectionFile;
        }
    }

    return $collections;
});
