<?php

return new \Phalcon\Config([
    'application' => [
        'controllersDir' => __DIR__ . '/../app/controllers/',
        'modelsDir'      => __DIR__ . '/../app/models/',
        'baseUri'        => getenv('APP_DOMAIN'),
    ]
]);
