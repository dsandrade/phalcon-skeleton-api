<?php

return new \Phalcon\Config([
    'driver'    => getenv('DATABASE_DRIVER'),
    'host'      => getenv('DATABASE_HOST'),
    'database'  => getenv('DATABASE_NAME'),
    'username'  => getenv('DATABASE_USERNAME'),
    'password'  => getenv('DATABASE_PASSWORD'),
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => ''
]);
