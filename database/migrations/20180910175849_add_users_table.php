<?php

use Phinx\Migration\AbstractMigration;

class AddUsersTable extends AbstractMigration
{
    public function up(): void
    {
        $exists = $this->hasTable('users');
        if (!$exists) {
            $table = $this->table(
                'users',
                [
                    'id'     => 'id',
                    'signed' => false,
                ]
            );

            $table
                ->addColumn(
                    'login',
                    'string',
                    [
                        'limit'   => 45,
                        'null'    => false,
                        'default' => '',
                    ]
                )
                ->addColumn(
                    'password',
                    'string',
                    [
                        'limit'   => 45,
                        'null'    => false,
                        'default' => '',
                    ]
                )
                ->addColumn(
                    'pubkey',
                    'string',
                    [
                        'limit'   => 255,
                        'null'    => false,
                        'default' => '',
                    ]
                )
                ->addIndex('login')
                ->addIndex('password')
                ->addIndex('pubkey')
                ->save();
        }
    }

    public function down(): void
    {
        $this->dropTable('users');
    }
}
