<?php

use Phinx\Migration\AbstractMigration;

class AddMessagesTable extends AbstractMigration
{
    public function up(): void
    {
        $exists = $this->hasTable('messages');
        if (!$exists) {
            $table = $this->table(
                'messages',
                [
                    'id'     => 'id',
                    'signed' => false,
                ]
            );

            $table
                ->addColumn(
                    'time',
                    'integer',
                    [
                        'limit'   => 11,
                        'null'    => false
                    ]
                )
                ->addColumn(
                    'id_sender',
                    'integer',
                    [
                        'limit'   => 11,
                        'null'    => false
                    ]
                )
                ->addColumn(
                    'id_receiver',
                    'integer',
                    [
                        'limit'   => 11,
                        'null'    => false
                    ]
                )
                ->addColumn(
                    'content',
                    'text',
                    [
                        'null'    => false,
                    ]
                )
                ->addColumn(
                    'type',
                    'boolean',
                    [
                        'null'    => false
                    ]
                )
                ->addIndex('time')
                ->addIndex('id_sender')
                ->addIndex('id_receiver')
                ->addIndex('type')
                ->save();
        }
    }

    public function down(): void
    {
        $this->dropTable('messages');
    }
}
