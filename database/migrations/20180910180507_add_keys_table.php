<?php

use Phinx\Migration\AbstractMigration;

class AddKeysTable extends AbstractMigration
{
    public function up(): void
    {
        $exists = $this->hasTable('keys');
        if (!$exists) {
            $table = $this->table(
                'keys',
                [
                    'id'     => 'id',
                    'signed' => false,
                ]
            );

            $table
                ->addColumn(
                    'time',
                    'integer',
                    [
                        'limit'   => 11,
                        'null'    => false
                    ]
                )
                ->addColumn(
                    'valid',
                    'boolean',
                    [
                        'null'    => false
                    ]
                )
                ->addColumn(
                    'key',
                    'string',
                    [
                        'limit'   => 255,
                        'null'    => false
                    ]
                )
                ->addIndex('time')
                ->addIndex('valid')
                ->addIndex('key')
                ->save();
        }
    }

    public function down(): void
    {
        $this->dropTable('keys');
    }
}
