<?php

/**
 * Collections let us define groups of routes that will all use the same controller.
 * We can also set the handler to be lazy loaded.  Collections can share a common prefix.
 * @var $exampleCollection
 */

// This is an Immediately Invoked Function in php.  The return value of the
// anonymous function will be returned to any file that "includes" it.
// e.g. $collection = include('example.php');

use Phalcon\Mvc\Micro\Collection;
use Phalcon\Skeleton\Api\Controllers\CoreController;

return call_user_func(function() {
    $core = new Collection();
    $core
        ->setPrefix('/api/v1')
        ->setHandler(CoreController::class)
        ->setLazy(true);

    // Set routers
    $core->get('/', 'index');

    return $core;
});
