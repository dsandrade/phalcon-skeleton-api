<?php

/**
 * Collections let us define groups of routes that will all use the same controller.
 * We can also set the handler to be lazy loaded.  Collections can share a common prefix.
 * @var $exampleCollection
 */

// This is an Immediately Invoked Function in php.  The return value of the
// anonymous function will be returned to any file that "includes" it.
// e.g. $collection = include('example.php');

use Phalcon\Mvc\Micro\Collection;
use Phalcon\Skeleton\Api\Controllers\MessagesController;

return call_user_func(function() {
    $messages = new Collection();

    // Set the handler & prefix
    $messages
        ->setHandler(MessagesController::class)
        ->setPrefix('/api/v1/messages')
        ->setLazy(true);

    // Set routers
    $messages->post('/', 'create');
    $messages->get('/{id_sender}/{id_receiver}', 'stream');
    $messages->get('/', 'inbox');

    return $messages;
});
