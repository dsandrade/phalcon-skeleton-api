<?php

/**
 * Collections let us define groups of routes that will all use the same controller.
 * We can also set the handler to be lazy loaded.  Collections can share a common prefix.
 * @var $exampleCollection
 */

// This is an Immediately Invoked Function in php.  The return value of the
// anonymous function will be returned to any file that "includes" it.
// e.g. $collection = include('example.php');

use Phalcon\Mvc\Micro\Collection;
use Phalcon\Skeleton\Api\Controllers\UsersController;

return call_user_func(function() {
    $users = new Collection();

    // Set the handler & prefix
    $users
        ->setPrefix('/api/v1/users')
        ->setHandler(UsersController::class)
        ->setLazy(true);

    // Set routers
    $users->post('/', 'create');
    $users->put('/{id}', 'update');
    $users->delete('/{id}', 'delete');
    $users->get('/', 'preview');
    $users->get('/{id}', 'info');

    return $users;
});
